package com.uptotech.testproject.daggerjava.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.uptotech.testproject.daggerjava.R;
import com.uptotech.testproject.daggerjava.api.NetworkUtils;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity {


    @Inject
    NetworkUtils networkUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Log.e("Diana", "networkUtils==null " + (networkUtils==null));
    }
}
