package com.uptotech.testproject.daggerjava.component;

import com.uptotech.testproject.daggerjava.App;
import com.uptotech.testproject.daggerjava.component.module.MainActivityModule;
import com.uptotech.testproject.daggerjava.component.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by DianaS on 28/02/2018.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        MainActivityModule.class,
        NetworkModule.class})
interface AppComponent extends AndroidInjector<App> {
}
