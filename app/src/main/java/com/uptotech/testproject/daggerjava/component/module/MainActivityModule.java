package com.uptotech.testproject.daggerjava.component.module;

import com.uptotech.testproject.daggerjava.ui.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by DianaS on 28/02/2018.
 */
@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector
    public abstract MainActivity contributeMainActivityInjector();
}
