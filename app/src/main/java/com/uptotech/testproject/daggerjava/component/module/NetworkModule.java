package com.uptotech.testproject.daggerjava.component.module;

import com.uptotech.testproject.daggerjava.api.NetworkUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by DianaS on 28/02/2018.
 */

@Module
public class NetworkModule {

    @Singleton
    @Provides
    NetworkUtils provideNetworkUtils() {
        return new NetworkUtils();
    }


}
